angular.module('ngBoilerplate.importBenchmark',['ui.router', 'ngResource', 'base64','checklist-model'])
.config(function($stateProvider) {
    $stateProvider.state('importBenchmark', {
            url:'/importBenchmark',
            views: {
                'main': {
                    templateUrl:'importBenchmark/importBenchmark.tpl.html',
                    controller: 'importBenchmarkCtrl'
                }
            },
            data : { pageTitle : "Import Benchmark" },
             resolve: {
                role: function(accountService) {
                    return accountService.getRole();
                },
                IsLoged: function(accountService) {
                    return accountService.isUserLoggedIn();
                },
                benchmarks: function(benchmarkService) {
                    return benchmarkService.getAllBenchmarksForThisUser();
                },
                directories: function(documentService) {
                    return documentService.getAllDirectoriesForThisUser();
                },
                analyzers: function(analyzerService) {
                    return analyzerService.getAllAnalyzers();
                }
            }
            }
    );
})
.factory('benchmarkService', function($resource) {
    var service = {};
    service.getAllBenchmarksForThisUser = function() {
     var Benchmark = $resource("/LuceneAnalyzerTester/rest/benchmark/getAll");
        return Benchmark.get().$promise.then(function(data) {
            return data.benchmarks;
          });
    };
    return service;
})
.factory('analyzerService', function($resource) {
    var service = {};
    service.getAllAnalyzers = function() {
     var Analyzer = $resource("/LuceneAnalyzerTester/rest/analyzer/getAll");
        return Analyzer.get().$promise.then(function(data) {
            return data.analyzers;
          });
    };
    return service;
})
.factory('benchmarkUploadService', function($resource) {
    var baseUrl = '/LuceneAnalyzerTester/rest/benchmark';
    return $resource(baseUrl, {},
        {
            add: {
                url: baseUrl + '/import',
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity,
                method: 'POST'
            }
        });
})
.controller("importBenchmarkCtrl", function($scope, $http, $state, benchmarkUploadService, role, IsLoged, directories, analyzers) {

    $scope.isLoggedIn = IsLoged;
    $scope.role = role;
    $scope.newBenchmark = {};
    $scope.analyzers = analyzers;
    $scope.testing = false;

    $scope.benchmark = {name: "", fileInput: ""};

    $scope.uploadBenchmark=function(){
        var formData = new FormData();
        formData.append('formDataJson', JSON.stringify($scope.touristObject));
        formData.append('documentsFile', documentsFile.files[0]);
        formData.append('matrixFile', matrixFile.files[0]);
        formData.append('queriesFile', queriesFile.files[0]);
        formData.append('name', $scope.benchmark.name);
        formData.append('analyzer_id', $scope.newBenchmark.analyzerType);
        formData.append('testing', $scope.testing);

        benchmarkUploadService.add(formData).$promise.then(function () {
                        alert("Success upload benchmarks");
                    }, function () {
                        alert('Error while uploading benchmarks!');
                    });


    };

	$scope.logout = function() {
		$scope.isLoggedIn = false;
		$http.post("/logout")
        .success(function () {
           $state.go("home");
        })
        .error(function (data) {
        });
    };
});
