angular.module('ngBoilerplate.testResults',['ui.router', 'ngResource', 'base64','checklist-model'])
.config(function($stateProvider) {
    $stateProvider.state('testResults', {
            url:'/testResults',
            views: {
                'main': {
                    templateUrl:'testResults/testResults.tpl.html',
                    controller: 'testResultsCtrl'
                }
            },
            data : { pageTitle : "Test Results" },
             resolve: {
                role: function(accountService) {
                    return accountService.getRole();
                },
                IsLoged: function(accountService) {
                    return accountService.isUserLoggedIn();
                },
                benchmarks: function(benchmarkService) {
                    return benchmarkService.getAllBenchmarksForThisUser();
                },
                directories: function(documentService) {
                    return documentService.getAllDirectoriesForThisUser();
                },
                analyzers: function(analyzerService) {
                    return analyzerService.getAllAnalyzers();
                },
                testingResultsAnalyzer: function(testResultsService) {
                    return testResultsService.getAllTestResultsForThisUser();
                }
            }
            }
    );
})
.factory('benchmarkService', function($resource) {
    var service = {};
    service.getAllBenchmarksForThisUser = function() {
     var Benchmark = $resource("/LuceneAnalyzerTester/rest/benchmark/getAll");
        return Benchmark.get().$promise.then(function(data) {
            return data.benchmarks;
          });
    };
    return service;
})
.factory('testResultsService', function($resource) {
    var service = {};
    service.getAllTestResultsForThisUser = function() {
     var TestingResultsService = $resource("/LuceneAnalyzerTester/rest/testingResults");
        return TestingResultsService.get().$promise.then(function(data) {
            return data.testingResultsAnalyzer;
          });
    };
    return service;
})
.factory('analyzerService', function($resource) {
    var service = {};
    service.getAllAnalyzers = function() {
     var Analyzer = $resource("/LuceneAnalyzerTester/rest/analyzer/getAll");
        return Analyzer.get().$promise.then(function(data) {
            return data.analyzers;
          });
    };
    return service;
})
.factory('benchmarkUploadService', function($resource) {
    var baseUrl = '/LuceneAnalyzerTester/rest/benchmark';
    return $resource(baseUrl, {},
        {
            add: {
                url: baseUrl + '/import',
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity,
                method: 'POST'
            }
        });
})
.controller("testResultsCtrl", function($scope, $http, $state, benchmarkUploadService, testingResultsAnalyzer, role, IsLoged, directories, analyzers) {

    $scope.isLoggedIn = IsLoged;
    $scope.role = role;
    $scope.newBenchmark = {};
    $scope.analyzers = analyzers;
    $scope.ready = true;
    $scope.testingResultsAnalyzer = testingResultsAnalyzer;
    $scope.testingQueries=[];

    $scope.benchmark = {name: "", fileInput: ""};

    $scope.uploadBenchmark=function(){
        var formData = new FormData();
        formData.append('formDataJson', JSON.stringify($scope.touristObject));
        formData.append('documentsFile', documentsFile.files[0]);
        formData.append('matrixFile', matrixFile.files[0]);
        formData.append('queriesFile', queriesFile.files[0]);
        formData.append('name', $scope.benchmark.name);
        formData.append('analyzer_id', $scope.newBenchmark.analyzerType);

        benchmarkUploadService.add(formData).$promise.then(function () {
                        alert("Success upload benchmarks");
                    }, function () {
                        alert('Error while uploading benchmarks!');
                    });


    };

    $scope.setSelectedResult = function (index, analysisResult) {
        $scope.testingQueries.splice(0,$scope.testingQueries.length);
        var i;
        for (i = 0; i < analysisResult.testingQueries.length; i++) {
            $scope.testingQueries.push(analysisResult.testingQueries[i]);
        }
    }

	$scope.logout = function() {
		$scope.isLoggedIn = false;
		$http.post("/logout")
        .success(function () {
           $state.go("home");
        })
        .error(function (data) {
        });
    };
});
