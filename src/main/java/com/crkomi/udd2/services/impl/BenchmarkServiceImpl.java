package com.crkomi.udd2.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.crkomi.udd2.entities.Account;
import com.crkomi.udd2.entities.Benchmark;
import com.crkomi.udd2.entities.RelevantDocument;
import com.crkomi.udd2.repositories.BenchmarkRepo;
import com.crkomi.udd2.repositories.PdfFileRepositiry;
import com.crkomi.udd2.repositories.QueryRepository;
import com.crkomi.udd2.rest.mvc.AnalyzerController;
import com.crkomi.udd2.rest.mvc.BenchmarkController;
import com.crkomi.udd2.rest.mvc.DocumentController;
import com.crkomi.udd2.services.BenchmarkService;
import com.crkomi.udd2.tools.SaxParser.DocumentsSaxHandler;
import com.crkomi.udd2.tools.SaxParser.MatrixSaxHandler;
import com.crkomi.udd2.tools.SaxParser.QueriesSaxHandler;
import com.crkomi.udd2.tools.models.BenchmarkModel;
import com.crkomi.udd2.tools.util.BenchmarkList;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.xml.parsers.*;

@Service
@Transactional
public class BenchmarkServiceImpl implements BenchmarkService {
	
	@Autowired
	private BenchmarkRepo repo;

	@Autowired
	ServletContext servletContext;

	@Autowired
	private QueryRepository queryRepository;

	@Autowired
	private DocumentController documentController;

	@Autowired
	private BenchmarkController benchmarkController;

	@Autowired
	private PdfFileRepositiry pdfFileRepositiry;

	@Autowired
	private EmailService emailService;

	@Autowired
	private AnalyzerController analyzerController;

	@Override
	public Benchmark createBenchmark(Benchmark data) {
		
		return repo.createBenchmark(data);
	}

	@Override
	public List<RelevantDocument> getAllRelevantDocs(long benchmarkId) {
		
		return repo.getAllRelevantDocs(benchmarkId);
	}

	@Override
	public RelevantDocument addRelevantDocument(RelevantDocument data) {
	
		return repo.addRelevantDocument(data);
	}

	@Override
	public Benchmark findBenchmark(long id) {
		
		return repo.findBenchmark(id);
	}

	@Override
	public Benchmark updateBenchmark(Benchmark data) {
		
		return repo.updateBenchmark(data);
	}

	@Override
	public Benchmark removeBenchmark(Benchmark data) {
		
		return repo.removeBenchmark(data);
	}

	@Override
	public void removeAllDocumentPaths(long benchmark_id) {
		repo.removeAllDocumentPaths(benchmark_id);		
	}

	@Override
	public void removeAllQueriesAndRelevantDocuments(long benchmark_id) {
		repo.removeAllQueriesAndRelevantDocuments(benchmark_id);		
	}

	@Override
	public void processingImportBenchmark(MultipartFile documentsFile, MultipartFile matrixFile, MultipartFile queriesFile, String benchmarkName, Account account, Long analyzerId, Boolean testing){
		createQueries(queriesFile, benchmarkName, account);
		System.out.println("<<<<<<<<<<<<<<<<Created Queries>>>>>>>>>>>>>>>>");
		createDirectory(documentsFile, benchmarkName, account);
		System.out.println("<<<<<<<<<<<<<<<<<<Created directory>>>>>>>>>>>>>>>>>>>>");
		Benchmark benchmark = createBenchmark(benchmarkName, analyzerId);
		System.out.println("<<<<<<<<<<<<<<<<<<<<Create benchmark>>>>>>>>>>>>>>>>>>>>>");
		createMatrix(matrixFile, benchmark, account, analyzerId);
		System.out.println("<<<<<<<<<<<<<<Create matrix>>>>>>>>>>>>>>");
		queryRepository.deleteAll();
		//emailService.sendMail(account.getEmail(), "Imported benchmark", "Benchmark '" + benchmark.getName() + "' successfully imported.");
		System.out.println("<<<<<<<<<<<<Imported benchmark>>>>>>>>>>>>>>>");
		System.out.println("<<<<<<<<<<<<Started testing>>>>>>>>>>>>>>");
		if(testing){
			testing(benchmark);
			//emailService.sendMail(account.getEmail(), "Tested benchmark", "Benchmark '" + benchmark.getName() + "' with analyzer '" +benchmark.getAnalyzerName() + "' successfully tested.");
			System.out.println("<<<<<<<<<<<<Tested benchmark>>>>>>>>>>>>>>>");
		}
	}

	/*
	private void createDirectory(MultipartFile documentsFile,String benchmarkName, Account account){
		String dir = account.getDirectoryPath();
		File newDir = new File(dir + File.separator + benchmarkName);

		String fileName = documentsFile.getOriginalFilename();

		File destFile = new File(newDir + File.separator + fileName);
		try {
			documentsFile.transferTo(destFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			File fXmlFile = destFile;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			Element element = doc.getDocumentElement();
			NodeList nodeListDocuments = element.getElementsByTagName("document");
			for(int i=0 ;i<nodeListDocuments.getLength();i++){
				Node nodeDocument = nodeListDocuments.item(i);
				NodeList nodeListSentences = nodeDocument.getChildNodes();
				com.itextpdf.text.Document document = new com.itextpdf.text.Document();
				PdfWriter.getInstance(document, new FileOutputStream(newDir + File.separator + nodeDocument.getAttributes().getNamedItem("id").getNodeValue() + ".pdf"));
				document.open();
				for(int j=0;j<nodeListSentences.getLength();j++){
					Node nodeSentence = nodeListSentences.item(j);

					Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
					Paragraph paragraph = new Paragraph(nodeSentence.getTextContent(), font);
					document.add(paragraph);
				}
				document.close();
				Path path = Paths.get(newDir + File.separator + nodeDocument.getAttributes().getNamedItem("id").getNodeValue() + ".pdf");
				String name = nodeDocument.getAttributes().getNamedItem("id").getNodeValue() + ".pdf";
				String originalFileName = nodeDocument.getAttributes().getNamedItem("id").getNodeValue() + ".pdf";
				String contentType = "text/plain";
				byte[] content = null;
				try{
					content = Files.readAllBytes(path);
					path.toFile().delete();
				}catch (final IOException e){

				}
				MultipartFile multipartFile = new MockMultipartFile(name, benchmarkName + "/" +originalFileName, contentType,content);
				documentController.uploadFile(new MultipartFile[] {multipartFile},"",benchmarkName);
			}
			destFile.delete();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	*/
	private void createDirectory(MultipartFile documentsFile,String benchmarkName, Account account) {
		String dir = account.getDirectoryPath();
		File newDir = new File(dir + File.separator + benchmarkName);

		String fileName = documentsFile.getOriginalFilename();

		File destFile = new File(newDir + File.separator + fileName);
		try {
			documentsFile.transferTo(destFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			DocumentsSaxHandler documentsSaxHandler = new DocumentsSaxHandler(newDir, benchmarkName, documentController, pdfFileRepositiry);
			saxParser.parse(destFile, documentsSaxHandler);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		destFile.delete();
	}

	private void createQueries(MultipartFile queriesFile, String benchmarkName, Account account){
		String dir = account.getDirectoryPath();
		File newDir = new File(dir + File.separator + benchmarkName);
		String fileName = queriesFile.getOriginalFilename();
		newDir.mkdirs();

		File destFile = new File(newDir + File.separator + fileName);
		try {
			queriesFile.transferTo(destFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			QueriesSaxHandler queriesSaxHandler = new QueriesSaxHandler(queryRepository);
			saxParser.parse(destFile, queriesSaxHandler);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		destFile.delete();
	}

	private Benchmark createBenchmark(String benchmarkName, Long analyzerId){
		BenchmarkModel benchmarkModel = new BenchmarkModel();
		benchmarkModel.setAnalyzerType(analyzerId);
		benchmarkModel.setName(benchmarkName);
		benchmarkModel.setDirectoryName(benchmarkName);
		try{
			benchmarkController.createBenchmark(benchmarkModel);
		}catch (Exception e) {

		}
		return repo.findBenchmarkByName(benchmarkName);
	}

	private void createMatrix(MultipartFile matrixFiles, Benchmark benchmark, Account account, Long analizerId){
		String dir = account.getDirectoryPath();
		File newDir = new File(dir + File.separator + benchmark.getName());
		String fileName = matrixFiles.getOriginalFilename();
		newDir.mkdirs();

		File destFile = new File(newDir + File.separator + fileName);
		try {
			matrixFiles.transferTo(destFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			MatrixSaxHandler matrixSaxHandler = new MatrixSaxHandler(pdfFileRepositiry, queryRepository, analizerId, benchmark, benchmarkController);
			saxParser.parse(destFile, matrixSaxHandler);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		destFile.delete();
	}

	private void testing(Benchmark selectedBenchmark){
		ResponseEntity<BenchmarkList> benchmarkListResponseEntity = benchmarkController.getAllBenchmarksForThisUser2();
		BenchmarkList benchmarkList = benchmarkListResponseEntity.getBody();
		BenchmarkModel selectedBenchmarkModel = null;
		for(BenchmarkModel benchmarkModel : benchmarkList.getBenchmarks()){
			if(benchmarkModel.getBenchmark_id() == selectedBenchmark.getBenchmark_id()){
				selectedBenchmarkModel = benchmarkModel;
				break;
			}
		}
		try{
			System.out.println("<<<<<<<<<<<<<<<<<<<<<<<Analyzer controller>>>>>>>>>>>>>>>>>>");
			analyzerController.testAnalyzer(selectedBenchmarkModel);
		}catch (Exception e){
			e.printStackTrace();
		}

	}
}
