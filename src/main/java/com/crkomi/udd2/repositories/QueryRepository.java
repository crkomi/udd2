package com.crkomi.udd2.repositories;

import com.crkomi.udd2.entities.Query;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QueryRepository extends JpaRepository<Query,Long> {
}
