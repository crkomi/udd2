package com.crkomi.udd2.repositories;

import com.crkomi.udd2.entities.TestingQueryTest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestingQueryRepository extends JpaRepository<TestingQueryTest,Long> {
}
