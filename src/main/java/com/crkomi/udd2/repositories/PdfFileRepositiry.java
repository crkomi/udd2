package com.crkomi.udd2.repositories;

import com.crkomi.udd2.entities.PdfFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PdfFileRepositiry extends JpaRepository<PdfFile,Long> {
}
