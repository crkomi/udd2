package com.crkomi.udd2.repositories;

import com.crkomi.udd2.entities.TestingResult;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestingResultRepository extends JpaRepository<TestingResult,Long> {
}
