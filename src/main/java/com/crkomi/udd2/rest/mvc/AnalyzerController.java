package com.crkomi.udd2.rest.mvc;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import com.crkomi.udd2.entities.*;
import com.crkomi.udd2.repositories.TestingQueryRepository;
import com.crkomi.udd2.repositories.TestingResultRepository;
import com.crkomi.udd2.services.AccountService;
import com.crkomi.udd2.services.AnalyzerService;
import com.crkomi.udd2.services.BenchmarkService;
import com.crkomi.udd2.tools.indexer.UDDIndexer;
import com.crkomi.udd2.tools.models.*;
import com.crkomi.udd2.tools.query.QueryBuilder;
import com.crkomi.udd2.tools.searcher.InformationRetriever;
import com.crkomi.udd2.tools.util.AnalysisResultList;
import com.crkomi.udd2.tools.util.AnalyzerList;
import com.crkomi.udd2.tools.util.SearchType;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Query;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


@Controller
@RequestMapping("/LuceneAnalyzerTester/rest/analyzer")
public class AnalyzerController {

    @Autowired
    ServletContext servletContext;

	@Autowired
	private AccountService accountService;
	
    @Autowired
	private BenchmarkService benchmarkService;
    
    @Autowired
	private AnalyzerService analyzerService;

    @Autowired
	private TestingResultRepository testingResultRepository;

    @Autowired
	private TestingQueryRepository testingQueryRepository;
    
    @RequestMapping(value = "/newAnalyzer", method = RequestMethod.POST)
	@PreAuthorize("hasRole('User','Admin')")
	public ResponseEntity<?> uploadAnalyzer(
			@RequestParam(value = "file", required = false) MultipartFile file, @RequestParam("formDataJson") String formDataJson,
			 @RequestParam("name") String analyzerName, @RequestParam("description") String description) throws COSVisitorException, IllegalStateException, IOException {
				
		//save jar file
		String analyzerRealPath = servletContext.getRealPath(File.separator + "analyzers") + File.separator + analyzerName;
		String fileName = file.getOriginalFilename();
		File destDir = new File(analyzerRealPath);
		if(!destDir.exists())
			destDir.mkdirs();
		File destFile = new File(destDir + File.separator + fileName);
		file.transferTo(destFile);		
				
		//save new analyzer info
		Analyzer newAnalyzer = new Analyzer();
		newAnalyzer.setName(analyzerName);
		newAnalyzer.setDescription(description);
		newAnalyzer.setPath(analyzerRealPath + File.separator + fileName);
		
		analyzerService.createAnalyzer(newAnalyzer);
		  	  
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	 
	@RequestMapping(value="/test",method = RequestMethod.POST)
	@PreAuthorize("hasRole('User','Admin')")
    public ResponseEntity<AnalysisResultList> testAnalyzer(@RequestBody BenchmarkModel benchmark) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		long startTesting = System.currentTimeMillis();
    	Benchmark benchmarkdb = benchmarkService.findBenchmark(benchmark.getBenchmark_id());
		//dynamically load selected analyzer
		com.crkomi.udd2.entities.Analyzer selectedAnalyzer = analyzerService.findAnalyzer(benchmark.getAnalyzerType());

		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		Account user = null;
		if (principal instanceof UserDetails) {
			UserDetails details = (UserDetails) principal;
			user = accountService.findAccountByUsername(details.getUsername());
		}

		TestingResult testingResult = new TestingResult();
		testingResult.setAnalyzerName(selectedAnalyzer.getName());
		testingResult.setBenchmarkName(benchmark.getName());
		testingResult.setAverageAccuracy(0.0f);
		testingResult.setAveragePrecision(0.0f);
		testingResult.setAverageF(0.0f);
		testingResult.setAverageRetrieval(0.0f);
		testingResult.setAccount(user);
		testingResult = testingResultRepository.save(testingResult);
		final Long testingResultId = testingResult.getId();
		URL[] urls = { new URL("jar:file:" + selectedAnalyzer.getPath()+"!/") };
		@SuppressWarnings("resource")
		URLClassLoader cl = new URLClassLoader(urls,Thread.currentThread().getContextClassLoader());		

		@SuppressWarnings("unchecked")
		Class<org.apache.lucene.analysis.Analyzer> classToLoad = (Class<org.apache.lucene.analysis.Analyzer>) cl.loadClass(selectedAnalyzer.getName());
		org.apache.lucene.analysis.Analyzer analyzer =  (org.apache.lucene.analysis.Analyzer) classToLoad.newInstance();
		System.out.println("<<<<<<<<<<Indexing is starting>>>>>>>>>>>>>>>");
		String indexDir = servletContext.getRealPath(File.separator + "indexes") + File.separator + System.currentTimeMillis();
		long startIndexing = System.currentTimeMillis();
		UDDIndexer UDDIndexer = new UDDIndexer(indexDir);
	    UDDIndexer.openIndexWriter(analyzer, indexDir);

	    for(DocumentPath documentPath: benchmarkdb.getAllDocumentsPath()) {
	    	File file = new  File(documentPath.getPath());
	    	UDDIndexer.index(file);
		
	    }

		UDDIndexer.closeIndexWriter();
		long endIndexing = System.currentTimeMillis();
		System.out.println("Indexing is duration: " + (endIndexing - startIndexing));
		List<SearchResultModel> analysisResultList = new ArrayList<SearchResultModel>();
		List<QueryAndRelevantDocumentsModel> queryAndRelevantDocumentsModels = benchmark.getQueryAndRelevantDocumentsList();

		List<String> allDocumentUID = new ArrayList<>();
		int countMaxDocument = UDDIndexer.getMaxDoc();
		int forCount = (countMaxDocument/1000);
		System.out.println("Get all document is started");
		for(int i=0;i<forCount ;i++) {
			Document[] allDocuments = UDDIndexer.get1000Documents(i);
			for(int j=0;j<1000;j++) {
				allDocumentUID.add(allDocuments[j].get("id"));
			}
		}
		for(int i=forCount * 1000; i< (forCount * 1000) + (countMaxDocument%1000);i++ ){
			allDocumentUID.add(UDDIndexer.getDocumentsAt(i).get("id"));
		}
		System.out.println("Get all documet is finished");

		final int countQueriesThread = queryAndRelevantDocumentsModels.size()/4;
		final ThreadResult threadResult1 = new ThreadResult();
		final ThreadResult threadResult2 = new ThreadResult();
		final ThreadResult threadResult3 = new ThreadResult();
		final ThreadResult threadResult4 = new ThreadResult();
		Thread t1 = new Thread(new Runnable() {
			public void run()
			{
				ThreadResult threadResultt = AnalyzerController.this.threadFunction(0,countQueriesThread,allDocumentUID, queryAndRelevantDocumentsModels,testingResultId,analyzer,indexDir);
				threadResult1.setAverageRetrieval(threadResultt.getAverageRetrieval());
				threadResult1.setAverageAccuracy(threadResultt.getAverageAccuracy());
				threadResult1.setAverageF(threadResultt.getAverageF());
				threadResult1.setAveragePrecision(threadResultt.getAveragePrecision());
			}});
		t1.start();
		Thread t2 = new Thread(new Runnable() {
			public void run()
			{
				ThreadResult threadResultt = AnalyzerController.this.threadFunction(countQueriesThread,2 * countQueriesThread,allDocumentUID, queryAndRelevantDocumentsModels,testingResultId,analyzer,indexDir);
				threadResult2.setAverageRetrieval(threadResultt.getAverageRetrieval());
				threadResult2.setAverageAccuracy(threadResultt.getAverageAccuracy());
				threadResult2.setAverageF(threadResultt.getAverageF());
				threadResult2.setAveragePrecision(threadResultt.getAveragePrecision());
			}});
		t2.start();
		Thread t3 = new Thread(new Runnable() {
			public void run()
			{
				ThreadResult threadResultt = AnalyzerController.this.threadFunction(2 * countQueriesThread,3 * countQueriesThread,allDocumentUID, queryAndRelevantDocumentsModels,testingResultId,analyzer,indexDir);
				threadResult3.setAverageRetrieval(threadResultt.getAverageRetrieval());
				threadResult3.setAverageAccuracy(threadResultt.getAverageAccuracy());
				threadResult3.setAverageF(threadResultt.getAverageF());
				threadResult3.setAveragePrecision(threadResultt.getAveragePrecision());
			}});
		t3.start();
		Thread t4 = new Thread(new Runnable() {
			public void run()
			{
				ThreadResult threadResultt = AnalyzerController.this.threadFunction(3 * countQueriesThread,queryAndRelevantDocumentsModels.size(),allDocumentUID, queryAndRelevantDocumentsModels,testingResultId,analyzer,indexDir);
				threadResult4.setAverageRetrieval(threadResultt.getAverageRetrieval());
				threadResult4.setAverageAccuracy(threadResultt.getAverageAccuracy());
				threadResult4.setAverageF(threadResultt.getAverageF());
				threadResult4.setAveragePrecision(threadResultt.getAveragePrecision());
			}});
		t4.start();

		try {
			t1.join();
			t2.join();
			t3.join();
			t4.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		AnalysisResultList list = new AnalysisResultList();
		list.setAnalysisResultList(analysisResultList);
	
		 File index = new File(indexDir);
		 if (index.exists()) {
		        File[] files = index.listFiles();
		        for (int i = 0; i < files.length; i++) {
		            if (files[i].isDirectory()) {
		                deleteDirectory(files[i]);
		            } else {
		                files[i].delete();
		            }
		        }
		    }
		index.delete();
		testingResult.setAveragePrecision((threadResult1.getAveragePrecision() + threadResult2.getAveragePrecision() + threadResult3.getAveragePrecision() + threadResult4.getAveragePrecision()) / queryAndRelevantDocumentsModels.size());
		testingResult.setAverageF((threadResult1.getAverageF() + threadResult2.getAverageF() + threadResult3.getAverageF() + threadResult4.getAverageF()) / queryAndRelevantDocumentsModels.size());
		testingResult.setAverageAccuracy((threadResult1.getAverageAccuracy() + threadResult2.getAverageAccuracy() + threadResult3.getAverageAccuracy() + threadResult4.getAverageAccuracy()) / queryAndRelevantDocumentsModels.size());
		testingResult.setAverageRetrieval((threadResult1.getAverageRetrieval() + threadResult2.getAverageRetrieval() + threadResult3.getAverageRetrieval() + threadResult4.getAverageRetrieval()) / queryAndRelevantDocumentsModels.size());
		testingResultRepository.save(testingResult);

		long endTesting = System.currentTimeMillis();
		System.out.println("Testing during is: " + (endTesting-startTesting));
        return new ResponseEntity<AnalysisResultList>(list, HttpStatus.OK);
        
	}

	public ThreadResult threadFunction(int startIndex, int endIndex, List<String> allDocumentUID, List<QueryAndRelevantDocumentsModel> queryAndRelevantDocumentsModels, Long testingResultId, org.apache.lucene.analysis.Analyzer analyzer, String indexDir){
		ThreadResult threadResult = new ThreadResult();
    	int currentIteration = startIndex;
    	TestingResult testingResult = testingResultRepository.getOne(testingResultId);
		for(int i= startIndex;i<endIndex;i++) {
			QueryAndRelevantDocumentsModel queryAndRelevantDocumentsModel = queryAndRelevantDocumentsModels.get(i);
			currentIteration++;
			long startIteration = System.currentTimeMillis();

			System.out.println("Time: " + new Date());
			System.out.println("Max iteration: " + queryAndRelevantDocumentsModels.size());
			System.out.println("Current iteration: " + currentIteration);

			SearchModel searchModel = queryAndRelevantDocumentsModel.getSearchModel();
			String text = searchModel.getText();
			String textst = searchModel.getTextSearchType();
			SearchType.Type textSearchType = SearchType.getType(textst);

			try {
				Query query = null;
				if(!(text == null || text.equals(""))){
					query = QueryBuilder.buildQuery(textSearchType, "text", text, analyzer);
				}

				List<DocumentModel> resultDocs = InformationRetriever.getData(query, null, null, analyzer,indexDir);
				List<String> allRelavant = queryAndRelevantDocumentsModel.getRelevantDocuments();

				List<String> restNonRelevant = new ArrayList<String>();
				float numAllRetrieved = resultDocs.size();
				float numAllRelavant = allRelavant.size();
				float numOfRelRetrieved = 0;
				for(DocumentModel dm : resultDocs) {
					if(allRelavant.contains(dm.getUid()))
						numOfRelRetrieved++;
				}

				for(String docUid : allDocumentUID){
					if (!allRelavant.contains(docUid))
						restNonRelevant.add(docUid);
				}

				float correctness = 0;
				float TP = numOfRelRetrieved;
				float TN = 0;
				for(String nonRelevant : restNonRelevant) {
					boolean contains = false;
					if(nonRelevant != null) {
						for(DocumentModel dm : resultDocs) {
							if(nonRelevant.equals(dm.getUid())) {
								contains = true;
								break;
							}
						}
					}
					if(!contains) TN++;
				}

				float FP = 0;
				for(DocumentModel dm: resultDocs) {
					if(restNonRelevant.contains(dm.getUid()))
						FP++;
				}

				float FN = allRelavant.size() - numOfRelRetrieved;

				correctness = (TP+TN)/(TP+TN+FP+FN);

				float precision = 0;
				if(numAllRetrieved != 0)
					precision = numOfRelRetrieved/numAllRetrieved;
				else
					precision = 0;

				float retrieval = 0;
				if(numAllRelavant != 0)
					retrieval = numOfRelRetrieved/numAllRelavant;
				else
					retrieval = 0;

				float beta = 1;
				float F = 0;
				if(precision == 0 && retrieval == 0) {
					F = 0;
				}
				else
					F = (float) (((Math.pow(beta, 2)+1) * precision * retrieval)/(Math.pow(beta, 2)* precision + retrieval));
				AnalysisResultModel analysisResultModel = new AnalysisResultModel();
				analysisResultModel.setPrecision(precision*100);
				analysisResultModel.setRetrieval(retrieval*100);
				analysisResultModel.setCorrectness(correctness*100);
				//countQueries++;
				analysisResultModel.setFmera(F);

				TestingQueryTest testingQueryTest = new TestingQueryTest();
				testingQueryTest.setTestingResult(testingResult);
				testingQueryTest.setPrecisionn(precision*100);
				testingQueryTest.setRecallll(retrieval*100);
				testingQueryTest.setAccuracyyy(correctness*100);
				testingQueryTest.setfMera(F);
				testingQueryTest.setSearchQueryy(queryAndRelevantDocumentsModel.getSearchModel().getText());
				testingQueryTest.setSearchType(queryAndRelevantDocumentsModel.getSearchModel().getTextSearchType());
				testingQueryTest = testingQueryRepository.save(testingQueryTest);
				threadResult.setAveragePrecision(threadResult.getAveragePrecision() + testingQueryTest.getPrecisionn());
				threadResult.setAverageF(threadResult.getAverageF() + testingQueryTest.getfMera());
				threadResult.setAverageAccuracy(threadResult.getAverageAccuracy() + testingQueryTest.getAccuracyyy());
				threadResult.setAverageRetrieval(threadResult.getAverageRetrieval() + testingQueryTest.getRecallll());
				//testingResult.setAveragePrecision(testingResult.getAveragePrecision() + testingQueryTest.getPrecisionn());
				//testingResult.setAverageF(testingResult.getAverageF() + testingQueryTest.getfMera());
				//testingResult.setAverageAccuracy(testingResult.getAverageAccuracy() + testingQueryTest.getAccuracyyy());
				//testingResult.setAverageRetrieval(testingResult.getAverageRetrieval() + testingQueryTest.getRecallll());

				//SearchResultModel searchResultModel = new SearchResultModel();
				//searchResultModel.setAnalysisResultModel(analysisResultModel);
				//searchResultModel.setSearchModel(searchModel);
				//searchResultModel.setDocuments(resultDocs);

				//analysisResultList.add(searchResultModel);

			} catch (IllegalArgumentException e1) {

			} catch (ParseException e1) {

			}

			long endIteration = System.currentTimeMillis();
			System.out.println("teration during is: " + (endIteration-startIteration));

		}
		return threadResult;
	}
	
	@RequestMapping(value="/getAll",method = RequestMethod.GET)
	@PreAuthorize("hasRole('User','Admin')")
    public ResponseEntity<AnalyzerList> getAllAnalyzers() {
		
		List<Analyzer> analyzers = analyzerService.getAllAnalyzers();
		List<AnalyzerModel> analyzerModels = new ArrayList<AnalyzerModel>();
		for(Analyzer analyzer: analyzers) {
			AnalyzerModel analyzerModel = new AnalyzerModel();
			analyzerModel.setAnalyzer_Id(analyzer.getAnalyzer_Id());
			analyzerModel.setName(analyzer.getName());
			analyzerModel.setDescription(analyzer.getDescription());
			analyzerModel.setPath(analyzer.getPath());
			analyzerModels.add(analyzerModel);
		}

		AnalyzerList analyzerList = new AnalyzerList();
		analyzerList.setAnalyzers(analyzerModels);
		
		return new ResponseEntity<AnalyzerList>(analyzerList,HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/remove",method = RequestMethod.POST)
	@PreAuthorize("hasRole('User','Admin')")
    public ResponseEntity<String> removeAnalyzer(@RequestBody Long analyzer_Id) {
		
		Analyzer analyzer = analyzerService.findAnalyzer(analyzer_Id);
		String analyzerRealPath = servletContext.getRealPath(File.separator + "analyzers") + File.separator + analyzer.getName();
		File file = new File(analyzerRealPath);
	    if (file.exists()) {
		     File[] files = file.listFiles();
		     for (int i = 0; i < files.length; i++) {		          
		          files[i].delete();
		     }
		}
        file.delete();
		analyzerService.removeAnalyzer(analyzer);
		
		return new ResponseEntity<String>("Analyzer has been removed successfully", HttpStatus.OK);
	}
	
	private void deleteDirectory(File file) {
		 File[] files = file.listFiles();
	        for (int i = 0; i < files.length; i++) {
	            if (files[i].isDirectory()) {
	                deleteDirectory(files[i]);
	            } else {
	                files[i].delete();
	            }
	        }
	     file.delete();
	}
	
}
