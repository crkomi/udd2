package com.crkomi.udd2.rest.mvc;

import com.crkomi.udd2.entities.Account;
import com.crkomi.udd2.entities.TestWraper;
import com.crkomi.udd2.entities.TestingResult;
import com.crkomi.udd2.repositories.TestingQueryRepository;
import com.crkomi.udd2.repositories.TestingResultRepository;
import com.crkomi.udd2.services.AccountService;
import com.crkomi.udd2.util.AccountList;
import com.crkomi.udd2.util.AccountModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/LuceneAnalyzerTester/rest/testingResults")
public class TestingController {

    @Autowired
    ServletContext servletContext;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TestingResultRepository testingResultRepository;

    @Autowired
    private TestingQueryRepository testingQueryRepository;

    @RequestMapping(value="", method = RequestMethod.GET)
    @PreAuthorize("hasRole('User','Admin')")
    public ResponseEntity<TestWraper> findAllTestingResults() {

        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        Account user = null;
        if (principal instanceof UserDetails) {
            UserDetails details = (UserDetails) principal;
            user = accountService.findAccountByUsername(details.getUsername());
        }
        TestWraper testWraper = new TestWraper();
        testWraper.setTestingResultsAnalyzer(user.getTestingResults());
        return new ResponseEntity<TestWraper>(testWraper, HttpStatus.OK);

    }

}
