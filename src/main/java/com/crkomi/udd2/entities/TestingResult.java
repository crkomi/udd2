package com.crkomi.udd2.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TestingResult {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String benchmarkName;

    @Column
    private String analyzerName;

    @Column
    private Float averagePrecision;

    @Column
    private Float averageRetrieval;

    @Column
    private Float averageAccuracy;

    @Column
    private Float averageF;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "testingResult")
    private List<TestingQueryTest> testingQueries = new ArrayList<>();

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Account account;

    public TestingResult(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBenchmarkName() {
        return benchmarkName;
    }

    public void setBenchmarkName(String benchmarkName) {
        this.benchmarkName = benchmarkName;
    }

    public String getAnalyzerName() {
        return analyzerName;
    }

    public void setAnalyzerName(String analyzerName) {
        this.analyzerName = analyzerName;
    }

    public Float getAveragePrecision() {
        return averagePrecision;
    }

    public void setAveragePrecision(Float averagePrecision) {
        this.averagePrecision = averagePrecision;
    }

    public Float getAverageRetrieval() {
        return averageRetrieval;
    }

    public void setAverageRetrieval(Float averageRetrieval) {
        this.averageRetrieval = averageRetrieval;
    }

    public Float getAverageAccuracy() {
        return averageAccuracy;
    }

    public void setAverageAccuracy(Float averageAccuracy) {
        this.averageAccuracy = averageAccuracy;
    }

    public Float getAverageF() {
        return averageF;
    }

    public void setAverageF(Float averageF) {
        this.averageF = averageF;
    }

    public List<TestingQueryTest> getTestingQueries() {
        return testingQueries;
    }

    public void setTestingQueries(List<TestingQueryTest> testingQueries) {
        this.testingQueries = testingQueries;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
