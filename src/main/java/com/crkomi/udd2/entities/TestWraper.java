package com.crkomi.udd2.entities;

import java.util.ArrayList;
import java.util.List;

public class TestWraper {

    private List<TestingResult> testingResultsAnalyzer = new ArrayList<>();

    public List<TestingResult> getTestingResultsAnalyzer() {
        return testingResultsAnalyzer;
    }

    public void setTestingResultsAnalyzer(List<TestingResult> testingResultsAnalyzer) {
        this.testingResultsAnalyzer = testingResultsAnalyzer;
    }
}
