package com.crkomi.udd2.entities;

import javax.persistence.Column;

public class ThreadResult {

    private Float averagePrecision;
    private Float averageRetrieval;
    private Float averageAccuracy;
    private Float averageF;

    public ThreadResult(){
        averagePrecision = 0f;
        averageRetrieval = 0f;
        averageAccuracy = 0f;
        averageF = 0f;
    }

    public Float getAveragePrecision() {
        return averagePrecision;
    }

    public void setAveragePrecision(Float averagePrecision) {
        this.averagePrecision = averagePrecision;
    }

    public Float getAverageRetrieval() {
        return averageRetrieval;
    }

    public void setAverageRetrieval(Float averageRetrieval) {
        this.averageRetrieval = averageRetrieval;
    }

    public Float getAverageAccuracy() {
        return averageAccuracy;
    }

    public void setAverageAccuracy(Float averageAccuracy) {
        this.averageAccuracy = averageAccuracy;
    }

    public Float getAverageF() {
        return averageF;
    }

    public void setAverageF(Float averageF) {
        this.averageF = averageF;
    }
}
