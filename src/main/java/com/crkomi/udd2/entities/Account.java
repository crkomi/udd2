package com.crkomi.udd2.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
public class Account {
	
    @Id @GeneratedValue
    private Long account_id;
    
    private String username;
    
    private String password;
    
    private String firstName;
    
    private String lastName;
    
    private String role;
    
    private String status;

    private String email;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy="account")
	private Set<Benchmark> benchmarks = new HashSet<Benchmark>();
    
    private String directoryPath;

    @JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "account")
	private List<TestingResult> testingResults = new ArrayList<>();
    
	public Long getAccount_id() {
		return account_id;
	}
	public void setAccount_id(Long account_id) {
		this.account_id = account_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Set<Benchmark> getBenchmarks() {
		return benchmarks;
	}
	public void setBenchmarks(Set<Benchmark> benchmarks) {
		this.benchmarks = benchmarks;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDirectoryPath() {
		return directoryPath;
	}
	public void setDirectoryPath(String directoryPath) {
		this.directoryPath = directoryPath;
	}

	public List<TestingResult> getTestingResults() {
		return testingResults;
	}

	public void setTestingResults(List<TestingResult> testingResults) {
		this.testingResults = testingResults;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}