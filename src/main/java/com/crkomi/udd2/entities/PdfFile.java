package com.crkomi.udd2.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PdfFile {

    @Id
    private Long id;

    private String relevatnId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRelevatnId() {
        return relevatnId;
    }

    public void setRelevatnId(String relevatnId) {
        this.relevatnId = relevatnId;
    }
}
