package com.crkomi.udd2.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Query {

    @Id
    private Long id;

    private String query;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

}
