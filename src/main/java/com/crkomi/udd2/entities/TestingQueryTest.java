package com.crkomi.udd2.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class TestingQueryTest {

    @Id
    @GeneratedValue
    private Long id;

    private String searchQueryy;

    private String searchType;

    private Float precisionn;

    private Float recallll;

    private Float accuracyyy;

    private Float fMera;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private TestingResult testingResult;

    public TestingQueryTest(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSearchQueryy() {
        return searchQueryy;
    }

    public void setSearchQueryy(String searchQueryy) {
        this.searchQueryy = searchQueryy;
    }

    public Float getPrecisionn() {
        return precisionn;
    }

    public void setPrecisionn(Float precisionn) {
        this.precisionn = precisionn;
    }

    public Float getRecallll() {
        return recallll;
    }

    public void setRecallll(Float recallll) {
        this.recallll = recallll;
    }

    public Float getAccuracyyy() {
        return accuracyyy;
    }

    public void setAccuracyyy(Float accuracyyy) {
        this.accuracyyy = accuracyyy;
    }

    public Float getfMera() {
        return fMera;
    }

    public void setfMera(Float fMera) {
        this.fMera = fMera;
    }

    public TestingResult getTestingResult() {
        return testingResult;
    }

    public void setTestingResult(TestingResult testingResult) {
        this.testingResult = testingResult;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }
}
