package com.crkomi.udd2.tools.SaxParser;

import com.crkomi.udd2.controllers.InitController;
import com.crkomi.udd2.entities.PdfFile;
import com.crkomi.udd2.repositories.PdfFileRepositiry;
import com.crkomi.udd2.rest.mvc.DocumentController;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DocumentsSaxHandler extends DefaultHandler {

    private com.itextpdf.text.Document document;
    private File newDirectory;
    private boolean isSentence;
    private String pdfFullPaths;
    private String pdfName;
    private String benchmarkName;
    private DocumentController documentController;
    private boolean isWorking;
    private Long idLong;

    private PdfFileRepositiry pdfFileRepositiry;

    public DocumentsSaxHandler(File newDirectory, String benchmarkName, DocumentController documentController, PdfFileRepositiry pdfFileRepositiry){
        this.newDirectory = newDirectory;
        this.benchmarkName = benchmarkName;
        this.documentController = documentController;
        this.pdfFileRepositiry = pdfFileRepositiry;
        isWorking = true;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        if(qName.equalsIgnoreCase("sentence") && isWorking){
            isSentence = true;
        }else if (qName.equalsIgnoreCase("document") && isWorking) {
            String id = attributes.getValue("id");
            idLong = Long.parseLong(id);
            if(Integer.parseInt(id) == InitController.processingDocumment){
                isWorking = false;
                return;
            }
            document = new com.itextpdf.text.Document();
            try{
                pdfFullPaths = newDirectory + File.separator + id + ".pdf";
                pdfName = id + ".pdf";
                PdfWriter.getInstance(document, new FileOutputStream(pdfFullPaths));
                document.open();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("sentence") && isWorking) {
            isSentence = false;
        }else if(qName.equalsIgnoreCase("document") && isWorking){
            document.close();
            try {
                PDFParser parser = new PDFParser(new FileInputStream(pdfFullPaths));
                parser.parse();
                PDDocument pdf = parser.getPDDocument();
                PDDocumentInformation info = pdf.getDocumentInformation();
                String currentTimeMillisString = "" + System.currentTimeMillis();
                info.setCustomMetadataValue("id", "" + currentTimeMillisString);
                pdf.setDocumentInformation(info);
                pdf.save(pdfFullPaths);
                pdf.close();
                try{
                    PdfFile pdfFile = new PdfFile();
                    pdfFile.setId(idLong);
                    pdfFile.setRelevatnId(currentTimeMillisString);
                    pdfFileRepositiry.save(pdfFile);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }catch (Exception e){

            }

            /*
            Path path = Paths.get(pdfFullPaths);
            String name = pdfName;
            String originalFileName = pdfName;
            String contentType = "text/plain";
            byte[] content = null;
            try{
                content = Files.readAllBytes(path);
                path.toFile().delete();
                MultipartFile multipartFile = new MockMultipartFile(name, benchmarkName + "/" +originalFileName, contentType,content);
                documentController.uploadFile(new MultipartFile[] {multipartFile},"",benchmarkName);
            }catch (final Exception e){
                e.printStackTrace();
            }
            */
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if(isSentence){
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
            Paragraph paragraph = new Paragraph(new String(ch, start, length), font);
            try{
                document.add(paragraph);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}

