package com.crkomi.udd2.tools.SaxParser;

import com.crkomi.udd2.controllers.InitController;
import com.crkomi.udd2.entities.Benchmark;
import com.crkomi.udd2.entities.Query;
import com.crkomi.udd2.repositories.PdfFileRepositiry;
import com.crkomi.udd2.repositories.QueryRepository;
import com.crkomi.udd2.rest.mvc.BenchmarkController;
import com.crkomi.udd2.rest.mvc.DocumentController;
import com.crkomi.udd2.tools.models.QueryAndRelevantDocumentsModel;
import com.crkomi.udd2.tools.models.SearchModel;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MatrixSaxHandler extends DefaultHandler {

    private QueryAndRelevantDocumentsModel queryAndRelevantDocumentsModel = null;
    private QueryRepository queryRepository;
    private PdfFileRepositiry pdfFileRepositiry;
    private Long analyzerId;
    private Benchmark benchmark;
    private List<String> relevantDocuments;
    private BenchmarkController benchmarkController;

    public MatrixSaxHandler(PdfFileRepositiry pdfFileRepositiry, QueryRepository queryRepository, Long analyzerId, Benchmark benchmark, BenchmarkController benchmarkController){
        this.pdfFileRepositiry = pdfFileRepositiry;
        this.queryRepository = queryRepository;
        this.analyzerId = analyzerId;
        this.benchmark = benchmark;
        this.benchmarkController = benchmarkController;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        if(qName.equalsIgnoreCase("document")){
            String id = attributes.getValue("id");
            if(Integer.parseInt(id) < InitController.processingDocumment){
                relevantDocuments.add(pdfFileRepositiry.getOne(Long.parseLong(id)).getRelevatnId());
            }
        }else if (qName.equalsIgnoreCase("query")) {
            String id = attributes.getValue("id");
            queryAndRelevantDocumentsModel = new QueryAndRelevantDocumentsModel();
            queryAndRelevantDocumentsModel.setBenchmark_id(benchmark.getBenchmark_id());
            SearchModel searchModel = new SearchModel();
            searchModel.setAnalyzerId(analyzerId);
            searchModel.setText(queryRepository.getOne(Long.parseLong(id)).getQuery());
            searchModel.setIndexDir(benchmark.getIndexDir());
            searchModel.setTextSearchType("regular");
            queryAndRelevantDocumentsModel.setSearchModel(searchModel);
            relevantDocuments = new ArrayList<>();
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("query")) {
            queryAndRelevantDocumentsModel.setRelevantDocuments(relevantDocuments);
            benchmarkController.saveBenchmark(queryAndRelevantDocumentsModel);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

    }
}
