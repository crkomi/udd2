package com.crkomi.udd2.tools.SaxParser;

import com.crkomi.udd2.entities.Query;
import com.crkomi.udd2.repositories.QueryRepository;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class QueriesSaxHandler extends DefaultHandler {

    private Query query = null;
    private QueryRepository queryRepository;
    private boolean isQuery;

    public QueriesSaxHandler(QueryRepository queryRepository){
        this.queryRepository = queryRepository;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        if (qName.equalsIgnoreCase("query")) {
            String id = attributes.getValue("id");
            query = new Query();
            query.setId(Long.parseLong(id));
            isQuery = true;
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("query")) {
            queryRepository.save(query);
            isQuery = false;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if(isQuery){
            query.setQuery(new String(ch, start, length));
        }
    }
}
